﻿using PassportAppShared;
using PassportShared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PassportShared;

namespace PassportUploaderService
{
    public partial class PassportUploader : ServiceBase
    {
        #region Variables

        private static int counter;
        private static int OnlyForeign = int.Parse(ConfigurationManager.AppSettings["OnlyForeignBranches"]);
        private static int OnlyLocal = int.Parse(ConfigurationManager.AppSettings["OnlyLocalBranches"]);
        private static int OnlyPoll = int.Parse(ConfigurationManager.AppSettings["OnlyPoll"]);
        private static int DoNormalPoll = int.Parse(ConfigurationManager.AppSettings["DoNormalPoll"]);
        private static int NumberOfThreads = int.Parse(ConfigurationManager.AppSettings["NumberOfThreads"]);
        private static int NoPolling = int.Parse(ConfigurationManager.AppSettings["NoPolling"]);
        private static int OnlyForeignWithLocalPolling = int.Parse(ConfigurationManager.AppSettings["OnlyForeignWithLocalPolling"]);
        public static string branchConnectionString = ConfigurationManager.AppSettings["branchConnectionString"];
        public static int waitforxminutes = int.Parse(ConfigurationManager.AppSettings["waitforxminutes"]);
        public static int waitforxminutesbranch = int.Parse(ConfigurationManager.AppSettings["waitforxminutesbranchrepeat"]);
        private static List<Branch> branches = null;
        private static Thread mainThread;

        #endregion

        #region Constructor

        public PassportUploader()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                CreateLog();
                eventLog1.WriteEntry("Failed to start. Please view 'C:/IrisLogs' for details");
            }
        } 

        #endregion

        #region Service

        protected override void OnStart(string[] args)
        {
            try
            {
                CreateLog();

                mainThread = null;

                Task t = Task.Run(() => 
                {
                    mainThread = Thread.CurrentThread;
                    BeginProcess();
                });                

                eventLog1.WriteEntry("PassportUploader service started");
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, null);
                eventLog1.WriteEntry("Failed to start. Please view 'C:/IrisLogs' for details");
            }
        }

        private void CreateLog()
        {
            eventLog1 = new System.Diagnostics.EventLog();

            if (!System.Diagnostics.EventLog.SourceExists("PassportUploaderSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "PassportUploaderSource", "PassportUploaderLog");
            }
            eventLog1.Source = "PassportUploaderSource";
            eventLog1.Log = "PassportUploaderLog";
        }

        protected override void OnStop()
        {
            try
            {
                Thread.Sleep(10);
                mainThread.Abort();

                eventLog1.WriteEntry("PassportUploader service stopped");
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("Failed to stop. Please view 'C:/IrisLogs' for details");
            }

        } 

        #endregion

        #region Application

        public static void BeginProcess()
        {
            try
            {
                // Get all branches
                if (branches == null)
                    branches = DataCenter.GetAllBranches();

                // Divide into x number of lists
                List<IEnumerable<Branch>> lists = null;

                if (OnlyForeign == 1 && OnlyForeignWithLocalPolling == 0)
                    lists = SplitList<Branch>(branches.Where(b => b.IsForeign == 1), NumberOfThreads).ToList();
                else if (OnlyLocal == 1)
                    lists = SplitList<Branch>(branches.Where(b => b.IsForeign == 0), NumberOfThreads).ToList();
                else
                    lists = SplitList<Branch>(branches, NumberOfThreads).ToList();

                if (OnlyPoll == 1)
                {
                    Parallel.ForEach<IEnumerable<Branch>>(lists, list =>
                    {
                        //List<Branch> alivebranches = list.Where(b => string.IsNullOrEmpty(b.ErrorMessage)).ToList();
                        OnlyPollProcess(list);
                    });
                }
                else
                {
                    // Run on seperate thread
                    Parallel.ForEach<IEnumerable<Branch>>(lists, list =>
                    {
                        //List<Branch> alivebranches = list.Where(b => string.IsNullOrEmpty(b.ErrorMessage)).ToList();
                        if (OnlyForeignWithLocalPolling == 1)
                        {
                            foreach (Branch branch in list)
                            {
                                if (branch.IsForeign == 0)
                                {
                                    CheckBranches(branch);
                                    DataCenter.UpdateBranchStatusTable(new List<Branch>() { branch });
                                }
                                else
                                    ProcessBranches(branch);
                            }
                        }
                        else
                        {
                            foreach (Branch branch in list)
                            {
                                ProcessBranches(branch);
                            }
                        }
                    });

                    // Wait a lil bit then start again
                    Thread.Sleep(5000 * 60);
                    BeginProcess();
                }
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, null);
            }
        }

        private static void OnlyPollProcess(IEnumerable<Branch> list)
        {
            foreach (Branch branch in list)
            {
                CheckBranches(branch);

                if (branch.BranchCode == 477 || branch.BranchCode == 274)
                    ProcessBranches(branch);
                else
                    DataCenter.UpdateBranchStatusTable(new List<Branch>() { branch });
            }

            // Wait a lil bit then start again
            Thread.Sleep(1000 * 60 * waitforxminutes);
            OnlyPollProcess(list);
        }

        public static IEnumerable<IEnumerable<T>> SplitList<T>(IEnumerable<T> source, int size)
        {
            int i = 0;
            var splits = from item in source
                         group item by i++ % size into part
                         select part.AsEnumerable();
            return splits;
        }

        private static Branch CheckBranches(Branch branch)
        {
            branch.ConnectionString = string.Format("Server={0},1433; {1}", branch.BranchIP, branchConnectionString);
            branch.ErrorMessage = string.Empty;

            try
            {
                // Get branch info
                Branch result = DataCenter.GetBranchData(branch);

                // Clear branch error message if we get this far
                DataCenter.ClearBranchError(branch);

                return result;
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, branch.BranchName + " (" + branch.BranchCode + ") ");
                branch.ErrorMessage = ex.Message;
                return branch;
            }
        }

        private static int ProcessBranches(Branch branch)
        {
            branch.ConnectionString = string.Format("Server={0},1433; {1}", branch.BranchIP, branchConnectionString);

            try
            {
                // if (branch.YetToUploadRecords > 0 && branch.Status != -1)
                // Get data from View at branches
                List<Person> data = DataCenter.GetNextBatchFromBranch(branch);

                if (data.Count != 0)
                {
                    // Save records and then send confirmation
                    List<Response> reply = DataCenter.SavePassportData(data);

                    // Update the branch table to show results
                    DataCenter.SendResponseToBranch(reply, branch);

                    // Keep record of what you have pulled and from where
                    LogError.LogData(branch, data.Count);
                }

                if (NoPolling == 0 || DoNormalPoll == 1)
                {
                    // Checkbrach
                    CheckBranches(branch);

                    // Update dashboard
                    DataCenter.UpdateBranchStatusTable(new List<Branch>() { branch });
                }

                // Repeat until all data is pulled
                if (data.Count == 20)
                {
                    // Wait a bit then go again
                    Thread.Sleep(1000 * 60 * waitforxminutesbranch);
                    ProcessBranches(branch);
                }

                return 1;
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, branch.BranchName + " (" + branch.BranchCode + ") ");
                return 3;
            }
        }

        #endregion
    }
}
