﻿using System;
using System.Configuration;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Common;
using System.Data.OracleClient;
//using Oracle.DataAccess.Client;

namespace PassportUploaderService
{
    public static class OracleDataProv
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["OracleConnectionString"].ToString();
        private static string connectionString2 = ConfigurationManager.ConnectionStrings["OracleConnectionString2"].ToString();
        public static DbConnection connection;
        public static DbConnection connection2;
        public static DbCommand command;

        public static void CreateConnection()
        {
            if (connection == null)
            {
                try
                {
                    connection = new OracleConnection(connectionString);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: Create Connection");
                    throw ex;
                }
            }
        }

        public static void CreateConnection2()
        {
            if (connection2 == null)
            {
                try
                {
                    connection2 = new OracleConnection(connectionString2);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Create Connection2");
                    throw ex;
                }
            }
        }

        public static void CreateTransactionCommand()
        {
            if (command != null && command.Transaction != null && command.Transaction.Connection != null && command.Transaction.Connection.State == ConnectionState.Open)
                command.Transaction.Connection.Close();

            if (connection == null)
                CreateConnection();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            DbTransaction transaction = connection.BeginTransaction();

            command = connection.CreateCommand();
            command.Transaction = transaction;
        }

        public static int ExecuteQuery(IDataParameter[] parameters)
        {
            int iRowsAffected = -1;
            DbCommand cmd = command;

            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                if (parameters != null && parameters.Length > 0)
                {
                    cmd.Parameters.Clear();

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        if (parameters[i] != null)
                            cmd.Parameters.Add(parameters[i]);
                    }
                }

                iRowsAffected = cmd.ExecuteNonQuery();
            }
            catch
            { throw; }

            return iRowsAffected;
        }

        public static int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                if (connection == null)
                {
                    CreateConnection();
                }

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (DbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch
            { throw; }
            finally
            {
                connection.Close();
            }
            return iRowsAffected;
        }

        public static int ExecuteQuery2(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                if (connection2 == null)
                {
                    CreateConnection2();
                }

                if (connection2.State != ConnectionState.Open)
                    connection2.Open();

                using (DbCommand cmd = connection2.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = connection2;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch
            { throw; }
            finally
            {
                connection2.Close();
            }
            return iRowsAffected;
        }

        public static string ExecuteStoredProcedure(string procname, IDataParameter[] parameters)
        {
            try
            {
                if (connection2 == null)
                {
                    CreateConnection2();
                }                

                using (DbCommand cmd = connection2.CreateCommand())
                {
                    cmd.CommandText = procname;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection2;

                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }                   

                    // Add output parameter
                    OracleParameter output = new OracleParameter("OUTSTATUS", OracleType.VarChar, 3);
                    output.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(output);

                    if (connection2.State != ConnectionState.Open)
                        connection2.Open();
                    
                    cmd.ExecuteNonQuery();
                    return output.Value.ToString();
                }
            }
            catch
            { throw; }
            finally
            {
                connection2.Close();
            }
        }

        public static void RunProcedure(string strQuery, IDataParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (connection == null)
                {
                    CreateConnection();
                }

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (DbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    DbDataAdapter OracleDA = new OracleDataAdapter();
                    OracleDA.SelectCommand = cmd;
                    OracleDA.Fill(dataSet);
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void CloseQuery()
        {
            connection.Close();
        }
    }
}
