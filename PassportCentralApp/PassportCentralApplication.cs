﻿using PassportAppShared;
using PassportShared.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PassportShared;

namespace PassportCentralApp
{
    public class PassportCentralApplication
    {
        public static int Counter;
        public static int OnlyForeign = int.Parse(ConfigurationManager.AppSettings["OnlyForeignBranches"]);
        public static int OnlyLocal = int.Parse(ConfigurationManager.AppSettings["OnlyLocalBranches"]);
        public static int OnlyPoll = int.Parse(ConfigurationManager.AppSettings["OnlyPoll"]);
        public static int DoNormalPoll = int.Parse(ConfigurationManager.AppSettings["DoNormalPoll"]);
        public static int NumberOfThreads = int.Parse(ConfigurationManager.AppSettings["NumberOfThreads"]);
        public static int NoPolling = int.Parse(ConfigurationManager.AppSettings["NoPolling"]);
        public static int OnlyForeignWithLocalPolling = int.Parse(ConfigurationManager.AppSettings["OnlyForeignWithLocalPolling"]);
        public static string BranchConnectionString = ConfigurationManager.AppSettings["branchConnectionString"];
        public static int Waitforxminutes = int.Parse(ConfigurationManager.AppSettings["waitforxminutes"]);
        public static int Waitforxminutesbranch = int.Parse(ConfigurationManager.AppSettings["waitforxminutesbranchrepeat"]);

        public static bool Endprogram;
        
        public static List<Branch> Branches;

        private static void Main()
        {
            BeginProcess();
        }

        public static int BeginProcess()
        {
            while (Endprogram == false)
            {
                try
                {
                    // Get all branches
                    if (Branches == null)
                        Branches = DataCenter.GetAllBranches();

                    // Divide into x number of lists
                    List<IEnumerable<Branch>> lists;

                    if (OnlyForeign == 1 && OnlyForeignWithLocalPolling == 0)
                        lists = SplitList(Branches.Where(b => b.IsForeign == 1), NumberOfThreads).ToList();
                    else if (OnlyLocal == 1)
                        lists = SplitList(Branches.Where(b => b.IsForeign == 0), NumberOfThreads).ToList();
                    else
                        lists = SplitList(Branches, NumberOfThreads).ToList();

                    if (OnlyPoll == 1)
                    {
                        Parallel.ForEach(lists, list =>
                        {
                            //List<Branch> alivebranches = list.Where(b => string.IsNullOrEmpty(b.ErrorMessage)).ToList();
                            OnlyPollProcess((List<Branch>) list);
                        });
                    }
                    else
                    {
                        // Run on seperate thread
                        Parallel.ForEach(lists, list =>
                            {
                                //List<Branch> alivebranches = list.Where(b => string.IsNullOrEmpty(b.ErrorMessage)).ToList();
                                if (OnlyForeignWithLocalPolling == 1)
                                {
                                    foreach (Branch branch in list)
                                    {
                                        if (branch.IsForeign == 0)
                                        {
                                            CheckBranches(branch);
                                            DataCenter.UpdateBranchStatusTable(new List<Branch>() { branch });
                                        }
                                        else
                                            ProcessBranches(branch);
                                    }
                                }
                                else
                                {
                                    foreach (Branch branch in list)
                                    {
                                        ProcessBranches(branch);
                                    }
                                }
                            });

                        // Wait a lil bit then start again
                        Thread.Sleep(1000 * 60 * Waitforxminutes);
                        BeginProcess();
                    }
                }
                catch (Exception ex)
                {
                    LogError.LogTextError(ex, null, null);
                }
            }

            return 1;
        }

        private static void OnlyPollProcess(List<Branch> list)
        {
            while (true)
            {
                foreach (var branch in list)
                {
                    CheckBranches(branch);

                    if (branch.BranchCode == 477 || branch.BranchCode == 274)
                        ProcessBranches(branch);
                    else
                        DataCenter.UpdateBranchStatusTable(new List<Branch>() {branch});
                }

                // Wait a lil bit then start again
                Thread.Sleep(1000 * 60 * Waitforxminutes);
            }
        }

        public static IEnumerable<IEnumerable<T>> SplitList<T>(IEnumerable<T> source, int size)
        {
            int i = 0;
            var splits = from item in source
                         group item by i++ % size into part
                         select part.AsEnumerable();
            return splits;
        }

        private static void CheckBranches(Branch branch)
        {
            branch.ConnectionString = $"Server={branch.BranchIP},1433; {BranchConnectionString}";
            branch.ErrorMessage = string.Empty;

            try
            {
                // Get branch info
                DataCenter.GetBranchData(branch);

                // Clear branch error message if we get this far
                DataCenter.ClearBranchError(branch);
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, branch.BranchName + " (" + branch.BranchCode + ") ");
                branch.ErrorMessage = ex.Message;
            }
        }

        private static void ProcessBranches(Branch branch)
        {
            branch.ConnectionString = $"Server={branch.BranchIP},1433; {BranchConnectionString}";

            try
            {
                // if (branch.YetToUploadRecords > 0 && branch.Status != -1)
                    // Get data from View at branches
                var data = DataCenter.GetNextBatchFromBranch(branch);

                if (data.Count != 0)
                {
                    branch.ErrorMessage = string.Empty;

                    // Save records and then send confirmation
                    var reply = DataCenter.SavePassportData(data);

                    // Update the branch table to show results
                    DataCenter.SendResponseToBranch(reply, branch);                

                    // Keep record of what you have pulled and from where
                    LogError.LogData(branch, data.Count);                   
                }

                if (NoPolling == 0 || DoNormalPoll == 1)
                {
                    // Checkbrach
                    CheckBranches(branch);

                    // Update dashboard
                    DataCenter.UpdateBranchStatusTable(new List<Branch> { branch });
                }

                // Repeat until all data is pulled
                if (data.Count == 20)
                {
                    // Wait a bit then go again
                    Thread.Sleep(1000 * 60 * Waitforxminutesbranch);
                    ProcessBranches(branch);
                }
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, branch.BranchName + " (" + branch.BranchCode + ") ");
            }            
        }

        public static void EndProcess()
        {
            try
            {
                Endprogram = true;
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, string.Empty);
            }
        }
    }
}
