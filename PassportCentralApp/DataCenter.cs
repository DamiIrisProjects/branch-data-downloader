﻿using PassportAppShared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PassportShared.Entities;
//using Oracle.DataAccess.Client;
using System.Configuration;
using System.Data.OracleClient;
using PassportShared;

namespace PassportCentralApp
{
    public static class DataCenter
    {
        public static string ConnectionString { get; set; }
        public static int CreateViewIfNotExisting = int.Parse(ConfigurationManager.AppSettings["CreateViewIfNotExisting"]);


        #region Branch Level
        public static Branch GetBranchData(Branch branch)
        {
            MSQLdataProvider msqLdataProvider = new MSQLdataProvider(branch.ConnectionString);
            string query = @"select flag,count(formno) cnt from NGRMON.dbo.Loc_UpdateQue
                            group by flag order by flag";

            using (DataSet dset = new DataSet())
            {
                msqLdataProvider.RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    if (row["flag"].ToString() == "E20")
                        branch.IssuedRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U20")
                        branch.UploadedRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U21")
                        branch.QCchangedRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U31")
                        branch.ExistingRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U00")
                        branch.YetToUploadRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U51")
                        branch.MissingFace = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U30")
                        branch.QcUpdateSuccessful = int.Parse(row["cnt"].ToString());
                }

                return branch;
            }
        }

        public static List<Person> GetNextBatchFromBranch(Branch branch)
        {
            MSQLdataProvider msqLdataProvider = new MSQLdataProvider(branch.ConnectionString);
            List<Person> result = new List<Person>();

            string query = "SELECT * FROM NGRMON.dbo.Loc_PendingQue";

            try
            {
                using (DataSet dset = new DataSet())
                {
                    msqLdataProvider.RunProcedure(query, null, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        Person person = new Person();
                        person.FormNo = row["formno"].ToString();
                        person.Surname = row["surname"].ToString();
                        person.FirstName = row["firstname"].ToString();
                        person.MiddleName = row["middlename"].ToString();
                        person.Gender = row["gender"].ToString();
                        person.DateOfBirth = row["Birth_Date"].ToString();
                        person.DocumentNo = row["docno"].ToString();
                        person.PassportType = row["Passport_Type"].ToString();
                        person.IssuePlace = row["issueplace"].ToString();
                        person.IssueDate = row["Issue_date"].ToString();
                        person.ExpiryDate = row["Expiry_Date"].ToString();
                        person.FaceImage = string.IsNullOrEmpty(row["faceimage"].ToString()) ? null : (byte[])row["faceimage"];
                        person.SignImage = string.IsNullOrEmpty(row["signimage"].ToString()) ? null : (byte[])row["signimage"];
                        person.StageCode = row["docstagecode"].ToString();

                        result.Add(person);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid object name 'NGRMON.dbo.Loc_PendingQue'") && CreateViewIfNotExisting == 1)
                {
                    // If its a case of creation of the view !!! TEMPORARY WORKAROUND !!!
                    query = @"Create view [dbo].[Loc_PendingQue] as
                         select * from Loc_PendingUpdates
                         where docno in (select top 20 docno from loc_UpdateQue where flag in('','U00','Q21'))";

                    try
                    {
                        msqLdataProvider.ExecuteQuery(query, null);

                        // try again
                        GetNextBatchFromBranch(branch);
                    }
                    catch (Exception innerex)
                    {
                        // Log error
                        LogError.LogTextError(innerex, null, branch.BranchName + " (" + branch.BranchCode + ") ");
                    }
                }
                else
                {
                    // Log error
                    LogError.LogTextError(ex, null, branch.BranchName + " (" + branch.BranchCode + ") ");
                }
            }

            return result;
        }

        public static void SendResponseToBranch(List<Response> response, Branch branch)
        {
            MSQLdataProvider msqLdataProvider = new MSQLdataProvider(branch.ConnectionString);
            string query = "update NGRMON.dbo.loc_updateque set flag = @flag where docno = @docno";

            foreach (Response res in response)
            {
                IDataParameter[] param =
                {   
                    new SqlParameter("flag", res.Status),
                    new SqlParameter("docno", res.DocNo)
                };

                try
                {
                    msqLdataProvider.ExecuteQuery(query, param);
                }
                catch (Exception ex)
                {
                    // log error
                    LogError.LogTextError(ex, null, string.Empty);
                }
            }
        }

        #endregion

        #region Central

        public static List<Response> SavePassportData(List<Person> passportData)
        {
            List<Response> result = new List<Response>();

            foreach (Person person in passportData)
            {
                try
                {
                    // Add face parameter
                    OracleParameter inface = new OracleParameter("INFACEIMAGE", OracleType.Blob);
                    inface.IsNullable = true;
                    if (person.FaceImage == null)
                        inface.Value = DBNull.Value;
                    else
                        inface.Value = person.FaceImage;

                    // Add signature parameter
                    OracleParameter insig = new OracleParameter("INSIGNIMAGE", OracleType.Blob);
                    insig.IsNullable = true;
                    if (person.SignImage == null)
                        insig.Value = DBNull.Value;
                    else
                        insig.Value = person.SignImage;

                    // Add FLAG
                    OracleParameter inflag = new OracleParameter("INFLAG", OracleType.Int32, 1);
                    inflag.Value = 0;

                    IDataParameter[] param =
                    {   
                        new OracleParameter("INFORMNO",  person.FormNo),
                        new OracleParameter("INSURNAME",  person.Surname),
                        new OracleParameter("INFIRSTNAME",  person.FirstName),
                        new OracleParameter("INMIDDLENAME",  person.MiddleName),
                        new OracleParameter("INGENDER",  person.Gender),
                        new OracleParameter("INBIRTHDATE",  person.DateOfBirth),
                        new OracleParameter("INDOCNO",  person.DocumentNo),
                        new OracleParameter("INPASSPORTTYPE",  person.PassportType),
                        new OracleParameter("INISSUEPLACE",  person.IssuePlace),
                        new OracleParameter("INISSUEDATE",  person.IssueDate),
                        new OracleParameter("INEXPIRYDATE",  person.ExpiryDate),
                        inface, insig, inflag,
                        new OracleParameter("INDOCSTAGECODE",  person.StageCode)
                    };

                        
                    string response = OracleDataProv.ExecuteStoredProcedure("epms_wip_dat.SPINSERTINTOWIPDAT", param);
                    result.Add(new Response() { DocNo = person.DocumentNo, Status = response });
                }
                catch (Exception ex)
                {
                    LogError.LogTextError(ex, null, string.Empty);
                    result.Add(new Response() { DocNo = person.DocumentNo, Status = string.Empty });
                }
            }

            return result;
        }

        public static int UpdateBranchStatusTable(List<Branch> branches)
        {
            Branch currbranch = null;
            try
            {
                foreach (Branch branch in branches)
                {
                    currbranch = branch;
                    string errormsg = branch.ErrorMessage == null ? string.Empty : branch.ErrorMessage.Replace("'", "''");
                    string query = @"update idoc.passport_branches set issued_records = " + branch.IssuedRecords + ", last_update = '" + DateTime.Now.ToString("dd-MM-yyyy hh:mm tt") + "', ERROR_MSG = '" + errormsg + "', QCUPDATED_RECORDS = " + branch.QcUpdateSuccessful + ", MISSINGFACE_RECORDS = " + branch.MissingFace + ", TOUPLOAD_RECORDS = " + branch.YetToUploadRecords + ",  uploaded_records = " + branch.UploadedRecords + ", qcchanged_records = " + branch.QCchangedRecords + ", existing_records = " + branch.ExistingRecords + " where branch_code = " + branch.BranchCode;
                    OracleDataProv.ExecuteQuery(query, null);
                }

                return 1;
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, currbranch == null ? string.Empty : currbranch.BranchName + " (" + currbranch.BranchCode + ") ");
                return -1;
            }
        }

        public static int ClearBranchError(Branch branch)
        {
            try
            {
                string query = @"update idoc.passport_branches set ERROR_MSG = '' where branch_code = " + branch.BranchCode;
                OracleDataProv.ExecuteQuery(query, null);

                return 1;
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex, null, branch == null ? string.Empty : branch.BranchName + " (" + branch.BranchCode + ") ");
                return -1;
            }
        }

        public static List<Branch> GetAllBranches()
        {
            List<Branch> result = new List<Branch>();

            string query = "select branch_code, branch_name, is_foreign, branch_ip from idoc.passport_branches where branch_ip is not null";

            try
            {
                using (DataSet dset = new DataSet())
                {
                    OracleDataProv.RunProcedure(query, null, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        Branch branch = new Branch();
                        branch.BranchName = row["branch_name"].ToString();
                        branch.BranchCode = int.Parse(row["branch_code"].ToString());
                        branch.BranchIP = row["branch_ip"].ToString();
                        branch.IsForeign = int.Parse(row["is_foreign"].ToString());
                        result.Add(branch);
                    }
                }
            }
            catch (Exception ex)
            {
                // log error
                LogError.LogTextError(ex, null, string.Empty);
            }

            return result;
        }

        #endregion

        #region Helpers

        private static int ValidateData(Person person)
        {
            // Apparently just check if the face and the doc number is there
            if (string.IsNullOrEmpty(person.DocumentNo))
                return 3;

            if (person.FaceImage == null)
                return 4;

            return 0;
        }

        private static bool PassportExists(Person person)
        {
            string query = "select docno from epms_wip_dat.pendingupdates where docno = :docno";
            IDataParameter[] param =
            {   
                new OracleParameter("docno",  person.DocumentNo)
            };

            try
            {
                using (DataSet dset = new DataSet())
                {
                    OracleDataProv.RunProcedure(query, param, dset);

                    if (dset.Tables[0].Rows.Count != 0)
                        return true;
                }
            }
            catch (Exception ex)
            {
                // log error
                LogError.LogTextError(ex, null, string.Empty);
            }

            return false;
        }

        #endregion
        
        /*** Responses From Server ****
        
         * 1 = Success
         * 2 = Passport exists
         * 3 = Docnumber empty/null
         * 4 = Face empty/null
         * 5 = Exception logged
         
        */

        /*** Statuses Available ***
        
         * BatchSize 1 Upload data batch size
         * Enrol E20 Passport issued successfully
         * QC Q21 Passport QC status changed
         * Upload U00 Ready for upload
         * Upload U20 Upload successful
         * Upload U21 Upload failed
         * Upload U30 QC status update successful
         * Upload U31 Record already exist
         * Enrol U41 Missing or invalid Docno
         * Enrol U51 Missing or invalid face image
        
        */
    }
}
