﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
//using Oracle.DataAccess.Client;

namespace PassportCentralApp
{
    public static class OracleDataProv
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["OracleConnectionString"].ToString();
        private static readonly string ConnectionString2 = ConfigurationManager.ConnectionStrings["OracleConnectionString2"].ToString();
        public static DbConnection Connection;
        public static DbConnection Connection2;
        public static DbCommand Command;

        public static void CreateConnection()
        {
            if (Connection == null)
            {
                try
                {
                    Connection = new OracleConnection(ConnectionString);
                }
                catch (Exception)
                {
                    Console.WriteLine("Error: Create Connection");
                    throw;
                }
            }
        }

        public static void CreateConnection2()
        {
            if (Connection2 == null)
            {
                try
                {
                    Connection2 = new OracleConnection(ConnectionString2);
                }
                catch (Exception)
                {
                    Console.WriteLine("Create Connection2");
                    throw;
                }
            }
        }

        public static void CreateTransactionCommand()
        {
            if (Command?.Transaction?.Connection != null && Command.Transaction.Connection.State == ConnectionState.Open)
                Command.Transaction.Connection.Close();

            if (Connection == null)
                CreateConnection();

            if (Connection.State == ConnectionState.Closed)
                Connection.Open();

            DbTransaction transaction = Connection.BeginTransaction();

            Command = Connection.CreateCommand();
            Command.Transaction = transaction;
        }

        public static int ExecuteQuery(IDataParameter[] parameters)
        {
            int iRowsAffected = -1;
            DbCommand cmd = Command;

            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            if (parameters != null && parameters.Length > 0)
            {
                cmd.Parameters.Clear();

                for (int i = 0; i < parameters.Length; i++)
                {
                    if (parameters[i] != null)
                        cmd.Parameters.Add(parameters[i]);
                }
            }

            iRowsAffected = cmd.ExecuteNonQuery();

            return iRowsAffected;
        }

        public static int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                if (Connection == null)
                {
                    CreateConnection();
                }

                if (Connection.State != ConnectionState.Open)
                    Connection.Open();

                using (DbCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = Connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                Connection?.Close();
            }
            return iRowsAffected;
        }

        public static int ExecuteQuery2(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                if (Connection2 == null)
                {
                    CreateConnection2();
                }

                if (Connection2.State != ConnectionState.Open)
                    Connection2.Open();

                using (DbCommand cmd = Connection2.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = Connection2;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                Connection2?.Close();
            }
            return iRowsAffected;
        }

        public static string ExecuteStoredProcedure(string procname, IDataParameter[] parameters)
        {
            try
            {
                if (Connection2 == null)
                {
                    CreateConnection2();
                }                

                using (DbCommand cmd = Connection2.CreateCommand())
                {
                    cmd.CommandText = procname;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = Connection2;

                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }                   

                    // Add output parameter
                    OracleParameter output = new OracleParameter("OUTSTATUS", OracleType.VarChar, 3);
                    output.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(output);

                    if (Connection2.State != ConnectionState.Open)
                        Connection2.Open();
                    
                    cmd.ExecuteNonQuery();
                    return output.Value.ToString();
                }
            }
            finally
            {
                Connection2?.Close();
            }
        }

        public static void RunProcedure(string strQuery, IDataParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (Connection == null)
                {
                    CreateConnection();
                }

                if (Connection != null && Connection.State != ConnectionState.Open)
                    Connection.Open();

                using (DbCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    DbDataAdapter oracleDa = new OracleDataAdapter();
                    oracleDa.SelectCommand = cmd;
                    oracleDa.Fill(dataSet);
                    Connection.Close();
                }
            }
            finally
            {
                if (Connection != null && Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }
            }
        }

        public static void CloseQuery()
        {
            Connection.Close();
        }
    }
}
