﻿using System.Runtime.Serialization;

namespace PassportShared.Entities
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string DateOfBirth { get; set; }

        [DataMember]
        public string DocumentNo { get; set; }

        [DataMember]
        public string PassportType { get; set; }

        [DataMember]
        public string IssuePlace { get; set; }

        [DataMember]
        public string IssueDate { get; set; }

        [DataMember]
        public string ExpiryDate { get; set; }

        [DataMember]
        public byte[] FaceImage { get; set; }

        [DataMember]
        public byte[] SignImage { get; set; }
        
        #region Operations

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToLower().ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        #endregion
    }
}
