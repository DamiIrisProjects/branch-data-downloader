﻿using System.Runtime.Serialization;

namespace PassportShared.Entities
{
    [DataContract]
    public class Branch
    {
        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public int IsForeign { get; set; }

        [DataMember]
        public int YetToUploadRecords { get; set; }

        [DataMember]
        public int MissingFace { get; set; }

        [DataMember]
        public int QcUpdateSuccessful { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public int IssuedRecords { get; set; }

        [DataMember]
        public int UploadedRecords { get; set; }

        [DataMember]
        public int QCchangedRecords { get; set; }

        [DataMember]
        public int ExistingRecords { get; set; }

        [DataMember]
        public string BranchIP { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public int Status { get; set; }
    }
}
