﻿using System.Runtime.Serialization;

namespace PassportShared.Entities
{

    [DataContract]
    public class Response
    {
        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string DocNo { get; set; }

    }
}
