﻿using System;
using System.IO;
using System.Text;

namespace PassportShared
{
    public static class LogError
    {
        public static DateTime Today { get; set; }

        private static readonly object SyncObject = new object();
        private static readonly object SyncErrObject = new object();

        private static TextWriter _tw;

        public static void LogTextError(Exception ex, string path, string branchdetails)
        {
            bool write = true;            
            string errormessage = CreateExceptionString(ex);
            
            // for now
            if (errormessage.Contains("Connection Timeout Expired"))
            {
                errormessage = branchdetails + ": Connection Timeout Expired";
                write = false;
            }

            if (errormessage.Contains("Connect timeout occurred"))
            {
                errormessage = branchdetails + ": Connection Timeout Expired";
                write = false;
            }

            if (errormessage.Contains("Invalid object name 'Loc_PendingUpdates'"))
                errormessage = branchdetails + ": Invalid object name 'Loc_PendingUpdates'";

            if (errormessage.Contains("Invalid object name 'NGRMON.dbo.Loc_UpdateQue'"))
                errormessage = branchdetails + ": Invalid object name 'NGRMON.dbo.Loc_UpdateQue'";

            if (errormessage.Contains("invalid username/password; logon denied"))
                errormessage = branchdetails + ": invalid username/password; logon denied";
            
            if (errormessage.Contains("SELECT permission denied"))
                errormessage = branchdetails + ": SELECT permission denied on object 'Loc_UpdateQue', database 'NGRMON', owner 'dbo'.";

            if (errormessage.Contains("an error occurred during the pre-login handshake"))
                errormessage = branchdetails + ": An error occurred during the pre-login handshake";

            if (errormessage.Contains("Invalid object name 'NGRMON.dbo.Loc_PendingQue'"))
                errormessage = branchdetails + ": Invalid object name 'NGRMON.dbo.Loc_PendingQue'";
            
            if (errormessage.Contains("deadlocked on lock resources"))
                errormessage = branchdetails + ": Resource deadlock. (threads trying to access at the same time)";

            if (errormessage.Contains("invalid length for variable character string Source") || errormessage.Contains("invalid length for variable character stringSource"))
                errormessage = branchdetails + ": Invalid length for variable character string Source";

            if (errormessage.Contains("Timeout expired."))
            {
                errormessage = branchdetails + ": Connection Timeout Expired";
                write = false;
            }

            if (errormessage.Contains("The login failed"))
                errormessage = branchdetails + ": Login Failed";

            if (errormessage.Contains("The connection was not closed"))
            {
                errormessage = branchdetails + ": The connection was not closed";
                write = false;
            }

            if (errormessage.Contains("The connection is closed"))
            {
                errormessage = branchdetails + ": The connection is closed";
                write = false;
            }

            if (errormessage.Contains("Cannot open database requested in login 'NGRMON'"))
                errormessage = branchdetails + ": Branch not setup yet. Cannot open database requested in login 'NGRMON'";

            if (errormessage.Contains("A network-related or instance-specific error occurred while establishing"))
            {
                errormessage = branchdetails + ": Could not connect";
                write = false;
            }

            if (write)
            {
                string demarcation = "===========";
                string filename = DateTime.Today.ToString("yyyy-MM-dd") + "_ErrLog.txt";

                // If path is not specified
                if (string.IsNullOrEmpty(path))
                    path = "C:/IrisAppLogs";

                string fullpath = Path.Combine(path, filename);

                // Make sure path exists
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                // Save as textfile
                lock (SyncErrObject)
                {
                    using (_tw = new StreamWriter(fullpath, true))
                    {
                        _tw.WriteLine("");
                        _tw.WriteLine(DateTime.Now.ToString("hh:mm:ss tt"));
                        _tw.WriteLine(demarcation);
                        _tw.WriteLine(errormessage);
                        _tw.Close();
                    }
                }
            }
        }

        private static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }

        public static void LogData(Entities.Branch branch, int count)
        {
            string filename = DateTime.Today.ToString("yyyy-MM-dd") + "_UploadedFilesLog.txt";
            string path = "C:/IrisAppLogs";
            string demarcation = "===========";

            string fullpath = Path.Combine(path, filename);

            // Make sure path exists
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            // Save as textfile
            lock (SyncObject)
            {
                // Save as textfile
                lock (SyncErrObject)
                {
                    using (_tw = new StreamWriter(fullpath, true))
                    {
                        _tw.WriteLine("");
                        _tw.WriteLine(DateTime.Now.ToString("hh:mm:ss tt"));
                        _tw.WriteLine(demarcation);
                        _tw.WriteLine(DateTime.Now.ToString("hh:mm:ss tt") + " - " + branch.BranchName + " (" + branch.BranchCode + ") : +" + count + " records");
                        _tw.Close();
                    }
                }
            }
        }
    }
}
