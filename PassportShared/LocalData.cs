﻿using PassportAppShared;
using PassportShared.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PassportShared
{
    public class LocalData
    {
        public string ConnectionString { get; set; } 
        private readonly MSQLdataProvider _msqLdataProvider;

        public LocalData(string connStr)
        {
            _msqLdataProvider = new MSQLdataProvider(connStr);
        }

        public Branch GetBranchData(Branch branch)
        {
            string query = @"select flag,count(formno) cnt from NGRMON.dbo.Loc_UpdateQue
                            group by flag order by flag";

            using (DataSet dset = new DataSet())
            {
                _msqLdataProvider.RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    if (row["flag"].ToString() == "E20")
                        branch.IssuedRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U20")
                        branch.UploadedRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U21")
                        branch.QCchangedRecords = int.Parse(row["cnt"].ToString());

                    if (row["flag"].ToString() == "U31")
                        branch.ExistingRecords = int.Parse(row["cnt"].ToString());
                }

                return branch;
            }
        }

        public List<Person> GetNextBatch()
        {
            List<Person> result = new List<Person>();

            string query = "SELECT * FROM NGRMON.dbo.Loc_PendingQue";

            try
            {
                using (DataSet dset = new DataSet())
                {
                    _msqLdataProvider.RunProcedure(query, null, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        Person person = new Person();
                        person.FormNo = row["formno"].ToString();
                        person.Surname = row["surname"].ToString();
                        person.FirstName = row["firstname"].ToString();
                        person.MiddleName = row["middlename"].ToString();
                        person.Gender = row["gender"].ToString();
                        person.DateOfBirth = row["Birth_Date"].ToString();
                        person.DocumentNo = row["docno"].ToString();
                        person.PassportType = row["Passport_Type"].ToString();
                        person.IssuePlace = row["issueplace"].ToString();
                        person.IssueDate = row["Issue_date"].ToString();
                        person.ExpiryDate = row["Expiry_Date"].ToString();
                        person.FaceImage = (byte[])row["faceimage"];
                        person.SignImage = (byte[])row["signimage"];

                        result.Add(person);
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error
                LogError.LogTextError(ex, null, string.Empty);
            }
                
            return result;
        }

        public void UpdateStatus(Dictionary<string, int> response)
        {
            string query = "update NGRMON.dbo.loc_updateque set flag = @flag where docno = @docno";

            foreach (var pair in response)
            {
                string status;

                // Set response
                switch (pair.Value)
                {
                    case 1: status = "U20"; break;
                    case 2: status = "U31"; break;
                    case 3: status = "U41"; break;
                    case 4: status = "U51"; break;

                    // assume anything else is an error
                    default: status = "U21"; break;

                }

                IDataParameter[] param =
                {   
                    new SqlParameter("flag", status),
                    new SqlParameter("docno", pair.Key)
                };

                try
                {
                    _msqLdataProvider.ExecuteQuery(query, param);
                }
                catch (Exception ex)
                {
                    // log error
                    LogError.LogTextError(ex, null, string.Empty);
                }
            }
        }

        public List<string> GetQCrecordChanges()
        {
            List<string> result = new List<string>();

            string query = "SELECT docno FROM NGRMON.dbo.loc_updateque where flag = 'Q21'";

            try
            {
                using (DataSet dset = new DataSet())
                {
                    _msqLdataProvider.RunProcedure(query, null, dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        result.Add(row["docno"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error
                LogError.LogTextError(ex, null, string.Empty);
            }

            return result;
        }

        /*** Responses From Server ****
        
         * 1 = Success
         * 2 = Passport exists
         * 3 = Docnumber empty/null
         * 4 = Face empty/null
         * 5 = Exception logged
         
        */

        /*** Statuses Available ***
        
         * BatchSize 1 Upload data batch size
         * Enrol E20 Passport issued successfully
         * QC Q21 Passport QC status changed
         * Upload U00 Ready for upload
         * Upload U20 Upload successful
         * Upload U21 Upload failed
         * Upload U31 Record already exist
         * Enrol U41 Missing or invalid Docno
         * Enrol U51 Missing or invalid face image
        
        */       
    }
}
