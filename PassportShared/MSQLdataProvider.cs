﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PassportAppShared
{
    public class MSQLdataProvider
    {
        public string connectionString;
        public SqlConnection connection;
        public SqlCommand command;

        public MSQLdataProvider(string connstr)
        {
            connectionString = connstr;
        }

        public void CreateConnection()
        {
            if (connection == null)
            {
                try
                {
                    connection = new SqlConnection(connectionString);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void CreateTransactionCommand()
        {
            if (command != null && command.Transaction != null && command.Transaction.Connection != null && command.Transaction.Connection.State == ConnectionState.Open)
                command.Transaction.Connection.Close();

            if (connection == null)
                CreateConnection();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            SqlTransaction transaction = connection.BeginTransaction();

            command = connection.CreateCommand();
            command.Transaction = transaction;
        }

        public int ExecuteScalar(string strQuery)
        {
            // Make sure there is a connection before running this
            CheckConnection();

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = strQuery;
                var result = cmd.ExecuteScalar();

                return result == null ? 0 : int.Parse(result.ToString());
            }
        }   
        
        public int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                // Make sure there is a connection before running this
                CheckConnection();

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch
            { throw; }
            finally
            {
                connection.Close();
            }
            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, IDataParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                CheckConnection();

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    SqlDataAdapter OracleDA = new SqlDataAdapter();
                    OracleDA.SelectCommand = cmd;
                    OracleDA.Fill(dataSet);
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void CheckConnection()
        {
            if (connection == null)
            {
                CreateConnection();
            }
          
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

        }

        public void CloseQuery()
        {
            connection.Close();
        }
    }
}
